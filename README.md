# Programming Assignment

### Tic Tac Toe
Tic-tac-toe or Xs and Os is a popular two players game, it is represented in the interface 
`com.progressoft.inducrion.ttt.TicTacToeGame`.

A tic-tac-toe game has a board of 9 spaces, 3 rows and 3 columns, where a player can choose the space he/she wants to 
draw X or O on, the space is represented by the class `com.progressoft.inducrion.ttt.Space` where the top-left space has 
the values: row = 0 and column = 0. 

The game can be played by creating a new instance of `com.progressoft.inducrion.ttt.impl.SimpleTicTacToeGame` class. A 
newly created game starts with all the spaces empty. The `TicTacToeGame.play()` method should be called each time a 
player wants to play his/her turn passing the selected `Space` and either `X` or `O`. If the selected space is not 
empty, a `SpaceAlreadyUsedException` should be thrown. Each time the `TicTacToeGame.play()` method is called the result 
of the game represented by `com.progressoft.inducrion.ttt.GameResult` is returned.

The result of the game can be one of the following:
* **IN_PROGRESS**: the game still have empty spaces, but no one has won yet 
* **X_WON**: the player using `X` character was able to form 3 Xs horizontally, vertically or diagonally  
* **O_WON**: the player using `O` character was able to form 3 Xs horizontally, vertically or diagonally
* **TIE**: the game has no empty spaces and no one has won

The test class `com.progressoft.inducrion.ttt.TicTacToeGameTest` will also help you understand what your code is 
expected to do.


### Project Structure
This project is a maven project, you can start working on the project by importing it to your favorite IDE. The project 
contains some JUnit tests to help you verify your work. You can run these tests using the IDE or from the command line 
using the command `mvn clean test`.

 > Changing the Unit tests code is not allowed.


### Required Delivery
The current code does not compile, you need to write the code necessary to make it compile and to make all the tests in 
the class `com.progressoft.inducrion.ttt.TicTacToeGameTest` pass successfully without modifying the test class itself.


### Evaluation Criteria
* compiled code and all tests passing
* code quality and simplicity, you should follow Java coding conventions and try to find the simplest solution.
* code duplication, you should minimize duplication to the minimum


### Optional Delivery
Some players are so bad in playing the game, they are embarrassed to play with their friends, and they prefer to do some 
training playing with the computer.
 
Implement the interface `com.progressoft.inducrion.ttt.ComputerPlayer` that will help them use it for training. 