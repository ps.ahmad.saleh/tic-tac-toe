package com.progressoft.inducrion.ttt;

import com.progressoft.inducrion.ttt.impl.SimpleTicTacToeGame;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashSet;
import java.util.Set;

public class TicTacToeGameTest {

    private TicTacToeGame ticTacToeGame;

    @BeforeEach
    void setUp() {
        ticTacToeGame = new SimpleTicTacToeGame();
    }

    @Test
    void givenNewGame_shouldNotBeFinished() {
        Assertions.assertFalse(ticTacToeGame.isFinished());
    }

    @ParameterizedTest
    @MethodSource("getAllPoints")
    void givenNewGame_thenAllSpacesShouldNotBeUsed(Space space) {
        Assertions.assertFalse(ticTacToeGame.isUsed(space));
    }

    @Test
    void whenPlaySpecificPoint_thenPointShouldBeMarkedAsUsed() {
        Space space = new Space(1, 1);
        ticTacToeGame.play(space, TicTacToeGame.X);

        Assertions.assertTrue(ticTacToeGame.isUsed(space));
    }

    @Test
    void givenSpaceIsUsed_whenPlayOnSameSpace_thenShouldThrowException() {
        Space space = new Space(1, 1);
        ticTacToeGame.play(space, TicTacToeGame.X);

        Assertions.assertThrows(SpaceAlreadyUsedException.class, () -> ticTacToeGame.play(space, TicTacToeGame.O));
    }

    @Test
    void whenPlayingApointThatDoesNotFinishTheGame_thenResultShouldBeInProgress() {
        ticTacToeGame.play(new Space(1, 1), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 2), TicTacToeGame.O);
        ticTacToeGame.play(new Space(1, 0), TicTacToeGame.X);
        ticTacToeGame.play(new Space(0, 1), TicTacToeGame.O);

        Assertions.assertEquals(GameResult.IN_PROGRESS, ticTacToeGame.play(new Space(2, 2), TicTacToeGame.X));
    }

    @Test
    void givenTowHorizontalXs_whenPlayTheThirdInSameRow_thenXShouldWin() {
        ticTacToeGame.play(new Space(0, 0), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 0), TicTacToeGame.O);
        ticTacToeGame.play(new Space(0, 1), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 1), TicTacToeGame.O);

        GameResult gameResult = ticTacToeGame.play(new Space(0, 2), TicTacToeGame.X);

        Assertions.assertEquals(GameResult.X_WON, gameResult);
    }

    @Test
    void givenThreeHorizontalXs_thenGameShouldBeFinished() {
        ticTacToeGame.play(new Space(0, 0), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 0), TicTacToeGame.O);
        ticTacToeGame.play(new Space(0, 1), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 1), TicTacToeGame.O);
        ticTacToeGame.play(new Space(0, 2), TicTacToeGame.X);

        Assertions.assertTrue(ticTacToeGame.isFinished());
    }

    @Test
    void givenAllSpacesPlayedWithNoWinner_thenGameResultShouldBeTie() {
        ticTacToeGame.play(new Space(0, 0), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 1), TicTacToeGame.O);
        ticTacToeGame.play(new Space(0, 1), TicTacToeGame.X);
        ticTacToeGame.play(new Space(0, 2), TicTacToeGame.O);
        ticTacToeGame.play(new Space(2, 0), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 0), TicTacToeGame.O);
        ticTacToeGame.play(new Space(1, 2), TicTacToeGame.X);
        ticTacToeGame.play(new Space(2, 1), TicTacToeGame.O);

        GameResult gameResult = ticTacToeGame.play(new Space(2, 2), TicTacToeGame.X);

        Assertions.assertEquals(GameResult.TIE, gameResult);
    }

    @Test
    void givenAllSpacesPlayedWithNoWinner_thenGameShouldBeFinished() {
        ticTacToeGame.play(new Space(0, 0), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 1), TicTacToeGame.O);
        ticTacToeGame.play(new Space(0, 1), TicTacToeGame.X);
        ticTacToeGame.play(new Space(0, 2), TicTacToeGame.O);
        ticTacToeGame.play(new Space(2, 0), TicTacToeGame.X);
        ticTacToeGame.play(new Space(1, 0), TicTacToeGame.O);
        ticTacToeGame.play(new Space(1, 2), TicTacToeGame.X);
        ticTacToeGame.play(new Space(2, 1), TicTacToeGame.O);
        ticTacToeGame.play(new Space(2, 2), TicTacToeGame.X);

        Assertions.assertTrue(ticTacToeGame.isFinished());
    }

    private static Set<Space> getAllPoints() {
        Set<Space> allSpaces = new HashSet<>();
        allSpaces.add(new Space(0, 0));
        allSpaces.add(new Space(0, 1));
        allSpaces.add(new Space(0, 2));
        allSpaces.add(new Space(1, 0));
        allSpaces.add(new Space(1, 1));
        allSpaces.add(new Space(1, 2));
        allSpaces.add(new Space(2, 0));
        allSpaces.add(new Space(2, 1));
        allSpaces.add(new Space(2, 2));

        return allSpaces;
    }
}
