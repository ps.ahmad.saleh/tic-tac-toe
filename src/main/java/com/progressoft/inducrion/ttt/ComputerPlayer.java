package com.progressoft.inducrion.ttt;

public interface ComputerPlayer {

    Space play(TicTacToeGame ticTacToeGame);
}
