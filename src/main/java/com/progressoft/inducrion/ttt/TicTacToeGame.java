package com.progressoft.inducrion.ttt;

public interface TicTacToeGame {

    char X = 'X';
    char O = 'O';

    GameResult play(Space space, char c);

    boolean isFinished();

    boolean isUsed(Space space);
}
