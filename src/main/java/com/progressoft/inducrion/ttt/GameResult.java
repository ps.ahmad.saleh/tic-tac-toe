package com.progressoft.inducrion.ttt;

public enum GameResult {

    IN_PROGRESS, X_WON, O_WON, TIE
}
